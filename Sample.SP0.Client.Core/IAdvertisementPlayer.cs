﻿using System;

namespace Sample.SP0.Client.Core
{
    public interface IAdvertisementPlayer
    {
        event EventHandler InterstitialAdvertisementClosedEvent;

        bool IsInterstitialAdvertisementReady { get; }

        void PlayInterstitialAdvertisement();
    }
}
