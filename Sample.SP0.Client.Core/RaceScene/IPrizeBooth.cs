﻿namespace Sample.SP0.Client.Core.RaceScene
{
    public interface IPrizeBooth
    {
        int GetPrizeCoin();
    }
}
