﻿using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.Core.RaceScene
{
    public class RaceFinishedEventArgs : EventArgs
    {
        public IEnumerable<RaceRecord> RaceRecords;
    }
}
