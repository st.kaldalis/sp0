﻿using Sample.SP0.Client.Core.RaceScene.View;
using System;

namespace Sample.SP0.Client.Core.RaceScene
{
    public class BankruptEndingAnimationBoardController
    {
        private readonly ISceneSetter sceneSetter;
        private readonly IBankruptEndingAnimationBoard bankruptEndingAnimationBoard;

        public BankruptEndingAnimationBoardController(IBankruptEndingAnimationBoard bankruptEndingAnimationBoard, ISceneSetter sceneSetter)
        {
            this.bankruptEndingAnimationBoard = bankruptEndingAnimationBoard;
            this.sceneSetter = sceneSetter;
        }

        public void OnGoToTitleButtonClickedEvent(object sender, EventArgs eventArgs)
        {
            sceneSetter.SetTitleScene();
        }

        public void OnAnimationFinishedEvent(object sender, EventArgs eventArgs)
        {
            bankruptEndingAnimationBoard.ShowGoToTitleButton();
        }
    }
}
