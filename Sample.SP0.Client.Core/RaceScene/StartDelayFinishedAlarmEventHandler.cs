﻿using Sample.SP0.Client.Core.RaceScene.View;
using System;

namespace Sample.SP0.Client.Core.RaceScene
{
    public class StartDelayFinishedAlarmEventHandler
    {
        private readonly IStarter starter;
        private readonly ICamera camera;

        public StartDelayFinishedAlarmEventHandler(IStarter starter, ICamera camera)
        {
            this.starter = starter;
            this.camera = camera;
        }

        public void OnStartDelayFinishedAlarmEvent(object sender, EventArgs args)
        {
            starter.FireStartingGun();

            camera.PlayZoomOutAnimation();
            camera.FollowFrontRunner();
        }
    }
}
