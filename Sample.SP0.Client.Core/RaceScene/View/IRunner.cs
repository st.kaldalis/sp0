﻿namespace Sample.SP0.Client.Core.RaceScene.View
{
    public interface IRunner
    {
        void Run(float speed);
        void Wait();
        float GetSpeed();
    }
}
