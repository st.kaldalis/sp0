﻿namespace Sample.SP0.Client.Core.RaceScene.View
{
    public interface ICamera
    {
        void PlayZoomInToStarterAnimation();
        void PlayZoomOutAnimation();
        void FollowFrontRunner();
    }
}
