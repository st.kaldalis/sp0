﻿using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.Core.RaceScene.View
{
    public interface IRaceScene
    {
        event EventHandler ShowRaceResultBoardAnimationFinishedEvent;
        event EventHandler ReceivePrizeCoinAnimationFinishedEvent;
        void PlayShowRaceResultBoardAnimation(IEnumerable<RaceRecord> raceRecords);
        void PlayReceivePrizeCoinAnimation(int prizeCoin);
        void PlayBankruptEndingAnimation();
        void ShowStartNewRaceButton();
    }
}
