﻿using System;

namespace Sample.SP0.Client.Core.RaceScene.View
{
    public interface IStarter
    {
        event EventHandler StartingGunFiredEvent;

        void FireStartingGun();
    }
}
