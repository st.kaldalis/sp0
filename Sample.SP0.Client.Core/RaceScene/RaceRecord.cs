﻿namespace Sample.SP0.Client.Core.RaceScene
{
    public struct RaceRecord
    {
        public int Rank;
        public int Lane;
        public string Name;
        public float RunningTime;
    }
}
