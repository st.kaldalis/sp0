﻿using Sample.SP0.Client.Core.RaceScene.View;
using Sample.SP0.UserDataServer.Protocol;
using System;
using System.Linq;

namespace Sample.SP0.Client.Core.RaceScene
{
    public class RunnerController : IUpdatable
    {
        private readonly IRunner runner;
        private readonly UserDataServer.Protocol.RaceRecord raceRecord;
        private readonly ITime time;
        private bool isStartingGunFired;
        private float runningTime;
        private SectionRecord sectionRecord;
        private int sectionRecordIndex;

        public RunnerController(IRunner runner, UserDataServer.Protocol.RaceRecord raceRecord, ITime time)
        {
            this.runner = runner;
            this.raceRecord = raceRecord;
            this.time = time;
            isStartingGunFired = false;
            runningTime = 0.0f;
            sectionRecord = raceRecord.SectionRecords.FirstOrDefault();
            sectionRecordIndex = 0;
        }

        public void OnStartingGunFiredEvent(object sender, EventArgs args)
        {
            isStartingGunFired = true;
        }

        private float GetSpeed()
        {
            float time = runningTime - sectionRecord.StartTime;
            return sectionRecord.InitialSpeed + sectionRecord.Acceleration * time;
        }

        public void Update()
        {
            if (!isStartingGunFired)
                return;

            runningTime += time.GetDeltaTime();
            if (runningTime < raceRecord.ResponseTime)
                return;

            UpdateSectionRecord();

            runner.Run(GetSpeed());
        }

        private void UpdateSectionRecord()
        {
            if (sectionRecord.StartTime < runningTime && runningTime <= sectionRecord.EndTime)
                return;

            for (var i = sectionRecordIndex; i < raceRecord.SectionRecords.Count(); i++)
            {
                SectionRecord sectionRecord = raceRecord.SectionRecords.ElementAt(i);
                if (sectionRecord.StartTime < runningTime && runningTime <= sectionRecord.EndTime)
                {
                    this.sectionRecord = sectionRecord;
                    sectionRecordIndex = i;
                    return;
                }
            }
        }
    }
}
