﻿using Sample.SP0.Client.Core.RaceScene.View;
using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.Core.RaceScene
{
    public class RaceFinishedEventHandler
    {
        private readonly IRaceScene scene;

        private readonly int bettedLane;
        private IEnumerable<RaceRecord> raceRecords;
        private readonly IPrizeBooth prizeBooth;
        private readonly UserData userData;

        public RaceFinishedEventHandler(IRaceScene scene, IPrizeBooth prizeBooth, int bettedLane, UserData userData)
        {
            this.scene = scene;
            scene.ShowRaceResultBoardAnimationFinishedEvent += OnShowRaceResultBoardAnimationFinishedEvent;
            scene.ReceivePrizeCoinAnimationFinishedEvent += OnReceivePrizeCoinAnimationFinishedEvent;
            this.prizeBooth = prizeBooth;
            this.bettedLane = bettedLane;
            this.userData = userData;
        }

        public void OnRaceFinishedEvent(object sender, RaceFinishedEventArgs args)
        {
            raceRecords = args.RaceRecords;
            scene.PlayShowRaceResultBoardAnimation(args.RaceRecords);
        }

        private async void OnShowRaceResultBoardAnimationFinishedEvent(object sender, EventArgs args)
        {
            if (IsRunnerWon())
            {
                var coin = prizeBooth.GetPrizeCoin();

                userData.Wallet.Coin += coin;
                scene.PlayReceivePrizeCoinAnimation(coin);
            }
            else if (userData.IsBankruptUser())
            {
                await userData.Reset();

                scene.PlayBankruptEndingAnimation();
            }
            else
                scene.ShowStartNewRaceButton();
        }

        private void OnReceivePrizeCoinAnimationFinishedEvent(object sender, EventArgs e)
        {
            scene.ShowStartNewRaceButton();
        }

        private bool IsRunnerWon()
        {
            foreach (var record in raceRecords)
                if (record.Lane == bettedLane && record.Rank == 1)
                    return true;
            return false;
        }
    }
}
