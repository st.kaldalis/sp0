﻿namespace Sample.SP0.Client.Core
{
    public interface ITime
    {
        float GetDeltaTime();
        long GetCurrentTime();
    }
}
