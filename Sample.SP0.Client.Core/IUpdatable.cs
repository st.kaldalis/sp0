﻿namespace Sample.SP0.Client.Core
{
    public interface IUpdatable
    {
        void Update();
    }
}
