﻿using Sample.SP0.Client.Core.BettingScene.View;
using Sample.SP0.UserDataServer.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sample.SP0.Client.Core.BettingScene
{
    public class BettingSceneController
    {
        private readonly ISceneSetter sceneSetter;
        private readonly IEnumerable<Tuple<int, RunnerInformation>> runnerInformations;
        private readonly GameData gameData;
        private readonly IBettingScene bettingScene;
        private readonly IAdvertisementPlayer advertisementPlayer;
        private readonly UserData userData;
        private readonly LocalData localData;
        private readonly ITime time;
        private int selectedLane;
        private int bettedLane;
        private int bettedCoin;

        public BettingSceneController(ISceneSetter sceneSetter, IEnumerable<Tuple<int, RunnerInformation>> runnerInformations,
            GameData gameData, int selectedLane, IBettingScene bettingScene, IAdvertisementPlayer advertisementPlayer,
            UserData userData, LocalData localData, ITime time)
        {
            this.sceneSetter = sceneSetter;
            this.runnerInformations = runnerInformations;
            this.selectedLane = selectedLane;
            this.bettingScene = bettingScene;
            bettedLane = -1;
            bettedCoin = 0;
            this.gameData = gameData;
            this.advertisementPlayer = advertisementPlayer;
            this.userData = userData;
            this.localData = localData;
            this.time = time;
        }

        public void OnClickStartRaceButton(object sender, EventArgs args)
        {
            if (!IsBettedRunnerExist() || bettedCoin == 0)
            {
                OpenMessageBox("BET_BEFORE_RACE");
                return;
            }

            if (localData.LastAdsPlayedTime + gameData.GetAdsCoolTime() < time.GetCurrentTime()
                && advertisementPlayer.IsInterstitialAdvertisementReady)
            {
                advertisementPlayer.InterstitialAdvertisementClosedEvent += OnInterstitialAdsClosedEvent;

                localData.SetLastAdsPlayedTime(time.GetCurrentTime());
                advertisementPlayer.PlayInterstitialAdvertisement();
                return;
            }

            StartRace();
        }

        private bool IsBettedRunnerExist()
        {
            return bettedLane > 0;
        }

        private void OnInterstitialAdsClosedEvent(object sender, EventArgs args)
        {
            advertisementPlayer.InterstitialAdvertisementClosedEvent -= OnInterstitialAdsClosedEvent;

            StartRace();
        }

        private void StartRace()
        {
            userData.Wallet.Coin = userData.Wallet.Coin - bettedCoin;

            sceneSetter.SetRaceScene(runnerInformations, bettedLane, bettedCoin);
        }

        public void OnClickNextButton(object sender, EventArgs args)
        {
            selectedLane++;
            if (selectedLane > gameData.GetLaneCount())
                selectedLane = 1;

            OnSelectedRunnerIndexChanged();
        }

        public void OnClickPrevButton(object sender, EventArgs args)
        {
            selectedLane--;
            if (selectedLane < 1)
                selectedLane = gameData.GetLaneCount();

            OnSelectedRunnerIndexChanged();
        }

        private void OnSelectedRunnerIndexChanged()
        {
            var isBettedLane = bettedLane == selectedLane;
            bettingScene.SetVisibleBettedCoinContainer(isBettedLane);
            if (isBettedLane)
                bettingScene.SetBettedCoin(bettedCoin);

            bettingScene.SetRunnerInformation(selectedLane, runnerInformations.Where(entry => entry.Item1 == selectedLane).Select(entry => entry.Item2).First());
        }

        public void OnClickBettingButton(object sender, EventArgs args)
        {
            OpenBettingWindow();
        }

        private void OpenBettingWindow()
        {
            var bettingWindow = bettingScene.OpenBettingWindow();

            bettingWindow.CancelButtonClickedEvent += (sender, args) => { bettingWindow.Close(); };
            bettingWindow.BettingButtonClickedEvent += (sender, args) =>
            {
                var bettedCoin = bettingWindow.GetBettedCoin();

                if (bettedCoin == gameData.GetMinBet())
                    OpenMessageBox("BETTED_COIN_UNDER_MIN_BET");
                else if (bettedCoin > userData.Wallet.Coin)
                    OpenMessageBox("BETTED_COIN_BIGGER_THAN_COIN");
                else
                {
                    bettingWindow.Close();

                    bettedLane = selectedLane;
                    this.bettedCoin = bettedCoin;

                    bettingScene.SetVisibleBettedCoinContainer(true);
                    bettingScene.SetBettedCoin(bettedCoin);
                }
            };

            bettingWindow.SetBettedCoin(bettedLane == selectedLane && bettedCoin > 0 ? bettedCoin : 0);
        }

        private void OpenMessageBox(string messageTextID)
        {
            var messageBox = bettingScene.OpenMessageBox();
            messageBox.OKButtonClickedEvent += (sender, args) => { messageBox.Close(); };
            messageBox.SetMessage(messageTextID);
        }
    }
}
