﻿using System;

namespace Sample.SP0.Client.Core.BettingScene.View
{
    public interface IBettingWindow
    {
        event EventHandler CancelButtonClickedEvent;
        event EventHandler BettingButtonClickedEvent;

        int GetBettedCoin();
        void Close();
        void SetBettedCoin(int value);
    }
}
