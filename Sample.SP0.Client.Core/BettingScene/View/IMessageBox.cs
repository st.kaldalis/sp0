﻿using System;

namespace Sample.SP0.Client.Core.BettingScene.View
{
    public interface IMessageBox
    {
        event EventHandler OKButtonClickedEvent;

        void SetMessage(string textID);
        void Close();
    }
}
