﻿using Sample.SP0.UserDataServer.Protocol;

namespace Sample.SP0.Client.Core.BettingScene.View
{
    public interface IBettingScene
    {
        void SetVisibleBettedCoinContainer(bool isVisible);
        IBettingWindow OpenBettingWindow();
        void SetRunnerInformation(int lane, RunnerInformation runnerInformation);
        void SetBettedCoin(int bettedCoin);
        IMessageBox OpenMessageBox();
    }
}
