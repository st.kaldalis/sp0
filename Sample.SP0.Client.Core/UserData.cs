﻿using System.Threading.Tasks;

namespace Sample.SP0.Client.Core
{
    public abstract class UserData
    {
        public readonly Wallet Wallet;

        public UserData()
        {
            Wallet = new Wallet();
        }

        public bool IsBankruptUser()
        {
            return Wallet.Coin == 0;
        }

        public abstract Task Reset();

        public abstract Task Load();
    }
}
