﻿namespace Sample.SP0.Client.Core
{
    public abstract class LocalData
    {
        public string Locale { protected set; get; }

        public long LastAdsPlayedTime { protected set; get; }

        public abstract void SetLocale(string locale);

        public abstract void SetLastAdsPlayedTime(long lastPlayedTime);
    }
}
