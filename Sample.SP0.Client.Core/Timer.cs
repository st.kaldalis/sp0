﻿using System;

namespace Sample.SP0.Client.Core
{
    public class Timer : ITimer, IUpdatable
    {
        private float remainTime;
        private readonly ITime time;
        private readonly ILoop loop;

        public event EventHandler AlarmEvent;

        public Timer(ITime time, ILoop loop)
        {
            this.time = time;
            this.loop = loop;
        }

        public void Set(float time)
        {
            remainTime = time;
        }

        public void Start()
        {
            loop.Add(this);
        }

        public void Update()
        {
            if (remainTime <= 0.0f)
                return;

            remainTime -= time.GetDeltaTime();
            if (remainTime <= 0.0f)
            {
                loop.Remove(this);
                remainTime = 0.0f;

                AlarmEvent(this, EventArgs.Empty);
                AlarmEvent = (sender, args) => { };
            }
        }
    }
}
