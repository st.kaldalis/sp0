﻿using System;

namespace Sample.SP0.Client.Core.TitleScene.View
{
    public interface ISelectLocaleWindow
    {
        event EventHandler<LocaleButtonClickedEventArgs> LocaleButtonClickedEvent;
        event EventHandler OKButtonClickedEvent;

        string GetSelectedLocale();
        void Close();
        void SelectLocale(string locale);
    }
}
