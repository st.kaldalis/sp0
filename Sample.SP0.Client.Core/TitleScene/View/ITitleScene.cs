﻿namespace Sample.SP0.Client.Core.TitleScene.View
{
    public interface ITitleScene
    {
        ISelectLocaleWindow OpenSelectLocaleWindow();
        void ShowStartButton();
    }
}
