﻿using Sample.SP0.Client.Core.TitleScene.View;
using System;

namespace Sample.SP0.Client.Core.TitleScene
{
    public class SelectLocalWindowController
    {
        private readonly ISelectLocaleWindow selectLocaleWindow;
        private readonly LocalData localData;
        private readonly ITextSet textSet;

        public SelectLocalWindowController(ISelectLocaleWindow selectLocaleWindow, LocalData localData, ITextSet textSet)
        {
            this.selectLocaleWindow = selectLocaleWindow;
            this.localData = localData;
            this.textSet = textSet;
        }

        public void OnClickOKButton(object sender, EventArgs eventArgs)
        {
            selectLocaleWindow.Close();
        }

        public void OnClickLocaleButton(object sender, LocaleButtonClickedEventArgs eventArgs)
        {
            localData.SetLocale(eventArgs.Locale);
            textSet.SetLocale(localData.Locale);

            selectLocaleWindow.SelectLocale(localData.Locale);
        }
    }
}
