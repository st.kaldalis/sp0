﻿using Sample.SP0.Client.Core.TitleScene.View;
using System;

namespace Sample.SP0.Client.Core.TitleScene
{
    public class TitleSceneController
    {
        private readonly ISceneSetter sceneSetter;
        private readonly ITitleScene titleScene;
        private readonly LocalData localData;
        private readonly ITextSet textSet;

        public TitleSceneController(ISceneSetter sceneSetter, ITitleScene titleScene, LocalData localData, ITextSet textSet)
        {
            this.sceneSetter = sceneSetter;
            this.localData = localData;
            this.titleScene = titleScene;
            this.textSet = textSet;
        }

        public void OnClickStartButton(object sender, EventArgs args)
        {
            sceneSetter.SetBettingScene();
        }

        public void OnClickLocaleButton(object sender, EventArgs args)
        {
            var window = titleScene.OpenSelectLocaleWindow();
            window.SelectLocale(localData.Locale);

            var ctrl = new SelectLocalWindowController(window, localData, textSet);
            window.LocaleButtonClickedEvent += ctrl.OnClickLocaleButton;
            window.OKButtonClickedEvent += ctrl.OnClickOKButton;
        }
    }
}
