﻿namespace Sample.SP0.Client.Core.TitleScene
{
    public interface ITextSet
    {
        void SetLocale(string locale);
    }
}
