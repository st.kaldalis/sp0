﻿using System;

namespace Sample.SP0.Client.Core.TitleScene
{
    public class LocaleButtonClickedEventArgs : EventArgs
    {
        public string Locale;
    }
}
