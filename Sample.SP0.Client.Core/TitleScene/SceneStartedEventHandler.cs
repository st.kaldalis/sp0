﻿using Sample.SP0.Client.Core.TitleScene.View;
using System;

namespace Sample.SP0.Client.Core.TitleScene
{
    public class SceneStartedEventHandler
    {
        private readonly ITitleScene titleScene;
        private readonly UserData userData;

        public SceneStartedEventHandler(ITitleScene titleScene, UserData userData)
        {
            this.titleScene = titleScene;
            this.userData = userData;
        }

        public async void OnSceneStartedEvent(object sender, EventArgs args)
        {
            await userData.Load();

            if (userData.IsBankruptUser())
                await userData.Reset();

            titleScene.ShowStartButton();
        }
    }
}
