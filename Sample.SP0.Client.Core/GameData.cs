﻿using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.Core
{
    public class GameData
    {
        public IEnumerable<string> GetRunnerIDs()
        {
            return new List<string> { "Runner00", "Runner01", "Runner02" };
        }

        public Tuple<int, int> GetConcentrationRange()
        {
            return new Tuple<int, int>(50, 100);
        }

        public Tuple<int, int> GetConditionRange()
        {
            return new Tuple<int, int>(50, 100);
        }

        public IEnumerable<string> GetRunnerNameAPartTextIDs()
        {
            var nameParts = new List<string> { "RUNNER_NAME_A_PART_0", "RUNNER_NAME_A_PART_1", "RUNNER_NAME_A_PART_2" };
            return nameParts;
        }

        public IEnumerable<string> GetRunnerNameBPartTextIDs()
        {
            var nameParts = new List<string> { "RUNNER_NAME_B_PART_0", "RUNNER_NAME_B_PART_1", "RUNNER_NAME_B_PART_2",
            "RUNNER_NAME_B_PART_3", "RUNNER_NAME_B_PART_4", "RUNNER_NAME_B_PART_5", "RUNNER_NAME_B_PART_6",
            "RUNNER_NAME_B_PART_7", "RUNNER_NAME_B_PART_8", "RUNNER_NAME_B_PART_9"};
            return nameParts;
        }

        public Tuple<int, int> GetStrengthRange()
        {
            return new Tuple<int, int>(50, 100);
        }

        public Tuple<int, int> GetStaminaRange()
        {
            return new Tuple<int, int>(50, 100);
        }

        public Tuple<int, int> GetWeightRange()
        {
            return new Tuple<int, int>(50, 100);
        }

        public int GetMinBet()
        {
            return 0;
        }

        public int GetLaneCount()
        {
            return 5;
        }

        public float GetStartDelay()
        {
            return 3.0f;
        }

        public int GetInitialCoin()
        {
            return 100;
        }

        public int GetAdsCoolTime()
        {
            return 180000;
        }
    }
}
