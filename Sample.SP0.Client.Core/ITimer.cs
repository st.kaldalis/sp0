﻿using System;

namespace Sample.SP0.Client.Core
{
    public interface ITimer
    {
        event EventHandler AlarmEvent;
        void Set(float time);
        void Start();
    }
}
