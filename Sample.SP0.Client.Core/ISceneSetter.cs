﻿using Sample.SP0.UserDataServer.Protocol;
using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.Core
{
    public interface ISceneSetter
    {
        void SetTitleScene();

        void SetBettingScene();

        void SetRaceScene(IEnumerable<Tuple<int, RunnerInformation>> runnerInformations, int bettedLane, int bettedCoin);
    }
}
