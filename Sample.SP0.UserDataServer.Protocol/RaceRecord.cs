﻿using System.Collections.Generic;

namespace Sample.SP0.UserDataServer.Protocol
{
    public struct RaceRecord
    {
        public int Lane;
        public float ResponseTime;
        public IEnumerable<SectionRecord> SectionRecords;
        public float RunningTime;
    }
}
