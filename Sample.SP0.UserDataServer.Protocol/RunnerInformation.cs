﻿namespace Sample.SP0.UserDataServer.Protocol
{
    public struct RunnerInformation
    {
        public string RunnerID;
        public string NameAPartTextID;
        public string NameBPartTextID;
        public int Condition;
        public int Weight;
        public int Stamina;
        public int Strength;
        public int Concentration;
        public float Odds;
    }
}
