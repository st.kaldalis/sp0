﻿using System;
using System.Collections.Generic;

namespace Sample.SP0.UserDataServer.Protocol
{
    public struct GetNewRaceResult
    {
        public bool IsSuccess;
        public IEnumerable<Tuple<int, RunnerInformation>> RunnerInformations;
    }
}
