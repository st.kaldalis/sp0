﻿namespace Sample.SP0.UserDataServer.Protocol
{
    public struct SectionRecord
    {
        public float InitialSpeed;
        public float Acceleration;
        public float StartTime;
        public float EndTime;
    }
}
