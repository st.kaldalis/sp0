﻿using System.Collections.Generic;

namespace Sample.SP0.UserDataServer.Protocol
{
    public struct GetRaceRecordsResult
    {
        public bool IsSuccess;

        public bool IsWin;
        public int Prize;
        public IEnumerable<RaceRecord> RaceRecords;

        public int Coin;
    }
}
