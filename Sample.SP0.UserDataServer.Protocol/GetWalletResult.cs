﻿namespace Sample.SP0.UserDataServer.Protocol
{
    public struct GetWalletResult
    {
        public bool IsSuccess;
        public int Coin;
    }
}
