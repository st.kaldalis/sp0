extends Node

var interstitial_ad : InterstitialAd
var interstitial_ad_load_callback := InterstitialAdLoadCallback.new()
var full_screen_content_callback := FullScreenContentCallback.new()
var on_initialization_complete_listener := OnInitializationCompleteListener.new()
	
signal initialize_finished()
signal load_interstitial_advertisement_finished(is_load_success)
signal interstitial_advertisement_dismissed()

func _ready():
	on_initialization_complete_listener.on_initialization_complete = _on_initialization_complete
	
	var request_configuration := RequestConfiguration.new()
	request_configuration.tag_for_child_directed_treatment = 1
	request_configuration.tag_for_under_age_of_consent = 1
	request_configuration.max_ad_content_rating = RequestConfiguration.MAX_AD_CONTENT_RATING_G
	request_configuration.test_device_ids = ["asdasda"]
	request_configuration.convert_to_dictionary()
	MobileAds.set_request_configuration(request_configuration)
	
	interstitial_ad_load_callback.on_ad_failed_to_load = on_interstitial_ad_failed_to_load
	interstitial_ad_load_callback.on_ad_loaded = on_interstitial_ad_loaded

	full_screen_content_callback.on_ad_clicked = func() -> void:
		print("on_ad_clicked")
	full_screen_content_callback.on_ad_dismissed_full_screen_content = func() -> void:
		print("on_ad_dismissed_full_screen_content")
		interstitial_advertisement_dismissed.emit()
		destroy()
		
	full_screen_content_callback.on_ad_failed_to_show_full_screen_content = func(ad_error : AdError) -> void:
		print("on_ad_failed_to_show_full_screen_content")
	full_screen_content_callback.on_ad_impression = func() -> void:
		print("on_ad_impression")
	full_screen_content_callback.on_ad_showed_full_screen_content = func() -> void:
		print("on_ad_showed_full_screen_content")

func initialize():
	MobileAds.initialize(on_initialization_complete_listener)
	
func _on_initialization_complete(initialization_status : InitializationStatus) -> void:
	print("MobileAds initialization complete")
	print_all_values(initialization_status)
	var ad_colony_app_options := AdColonyAppOptions.new()
	print("set values ad_colony")
	ad_colony_app_options.set_privacy_consent_string(AdColonyAppOptions.CCPA, "STRIaNG CCPA")
	ad_colony_app_options.set_privacy_framework_required(AdColonyAppOptions.CCPA, false)
	ad_colony_app_options.set_user_id("asdaaaad")
	ad_colony_app_options.set_test_mode(false)
	
	print(ad_colony_app_options.get_privacy_consent_string(AdColonyAppOptions.CCPA))
	print(ad_colony_app_options.get_privacy_framework_required(AdColonyAppOptions.CCPA))
	print(ad_colony_app_options.get_user_id())
	print(ad_colony_app_options.get_test_mode())
	
	if OS.get_name() == "iOS":
		#FBAdSettings is available only for iOS, Google didn't put this method on Android SDK
		FBAdSettings.set_advertiser_tracking_enabled(true)
		
	Vungle.update_ccpa_status(Vungle.Consent.OPTED_IN)
	Vungle.update_ccpa_status(Vungle.Consent.OPTED_OUT)
	Vungle.update_consent_status(Vungle.Consent.OPTED_IN, "message1")
	Vungle.update_consent_status(Vungle.Consent.OPTED_OUT, "message2")
	
	initialize_finished.emit()
	
func load_interstitial_advertisement():
	InterstitialAdLoader.new().load("ca-app-pub-3940256099942544/1033173712", AdRequest.new(), interstitial_ad_load_callback)

func on_interstitial_ad_failed_to_load(adError : LoadAdError) -> void:
	print(adError.message)
	
	load_interstitial_advertisement_finished.emit(false)
	
func on_interstitial_ad_loaded(interstitial_ad : InterstitialAd) -> void:
	print("interstitial ad loaded" + str(interstitial_ad._uid))
	interstitial_ad.full_screen_content_callback = full_screen_content_callback
	self.interstitial_ad = interstitial_ad
#
	load_interstitial_advertisement_finished.emit(true)

func print_all_values(initialization_status : InitializationStatus) -> void:
	for key in initialization_status.adapter_status_map:
		var adapterStatus : AdapterStatus = initialization_status.adapter_status_map[key]
		prints("Key:", key, "Latency:", adapterStatus.latency, "Initialization Status:", adapterStatus.initialization_status, "Description:", adapterStatus.description)

func destroy():
	if interstitial_ad:
		interstitial_ad.destroy()
		interstitial_ad = null #need to load again

func show_interstitial_advertisement():
	if interstitial_ad:
		interstitial_ad.show()
