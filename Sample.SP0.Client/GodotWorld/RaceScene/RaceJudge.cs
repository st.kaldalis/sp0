﻿using Sample.SP0.Client.Core;
using Sample.SP0.Client.Core.RaceScene;
using Sample.SP0.Client.GodotWorld.View;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sample.SP0.Client.GodotWorld.RaceScene
{
    public class RaceJudge : IUpdatable
    {
        public event EventHandler<RaceFinishedEventArgs> RaceFinishedEvent;

        private readonly IReadOnlyDictionary<int, Runner> runners;
        private readonly IReadOnlyDictionary<int, float> finalLineXPositions;
        private readonly HashSet<Runner> finishers;
        private readonly IEnumerable<UserDataServer.Protocol.RaceRecord> raceRecords;

        public RaceJudge(IReadOnlyDictionary<int, Runner> runners, IReadOnlyDictionary<int, float> finalLineXPositions, IEnumerable<UserDataServer.Protocol.RaceRecord> raceRecords)
        {
            finishers = new HashSet<Runner>();
            this.runners = runners;
            this.finalLineXPositions = finalLineXPositions;
            this.raceRecords = raceRecords;
        }

        private IEnumerable<Sample.SP0.Client.Core.RaceScene.RaceRecord> CreateRaceRecords()
        {
            var ranks = CreateRanks();
            var runningTimes = CreateRunningTimes();

            var raceRecords = new List<Sample.SP0.Client.Core.RaceScene.RaceRecord>();
            foreach (var entry in runners)
            {
                var raceRecord = new Sample.SP0.Client.Core.RaceScene.RaceRecord();
                raceRecord.Lane = entry.Key;
                raceRecord.Name = entry.Value.RunnerName;
                raceRecord.RunningTime = runningTimes.Where(item => item.Item1 == entry.Key)
                                                     .Select(item => item.Item2)
                                                     .FirstOrDefault();
                raceRecord.Rank = ranks.Where(item => item.Item1 == entry.Key)
                                       .Select(item => item.Item2)
                                       .FirstOrDefault();

                raceRecords.Add(raceRecord);
            }

            return raceRecords.OrderBy(item => item.Rank);
        }

        private List<Tuple<int, int>> CreateRanks()
        {
            var rank = 1;
            var ranks = new List<Tuple<int, int>>();
            foreach (var raceRecord in raceRecords.OrderBy(item => item.RunningTime))
                ranks.Add(new Tuple<int, int>(raceRecord.Lane, rank++));
            return ranks;
        }

        private List<Tuple<int, float>> CreateRunningTimes()
        {
            var runningTimes = new List<Tuple<int, float>>();
            foreach (var raceRecord in raceRecords)
                runningTimes.Add(new Tuple<int, float>(raceRecord.Lane, raceRecord.RunningTime));
            return runningTimes;
        }

        public void Update()
        {
            foreach (KeyValuePair<int, Runner> entry in runners)
            {
                Runner runner = entry.Value;
                if (finishers.Contains(runner))
                    continue;
                if (runner.Position.X >= finalLineXPositions[entry.Key])
                    finishers.Add(runner);
            }

            if (finishers.Count == runners.Count())
            {
                var evtArgs = new RaceFinishedEventArgs
                {
                    RaceRecords = CreateRaceRecords()
                };
                RaceFinishedEvent(this, evtArgs);
            }
        }
    }
}
