﻿using Sample.SP0.Client.Core.RaceScene;

namespace Sample.SP0.Client.GodotWorld.RaceScene
{
    public class PrizeBooth : IPrizeBooth
    {
        private readonly int prize;

        public PrizeBooth(int prize)
        {
            this.prize = prize;
        }

        public int GetPrizeCoin()
        {
            return prize;
        }
    }
}
