﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class RaceResultBoard : Control
    {
        public event EventHandler ShowAnimationFinishedEvent;
        public event EventHandler StartNewRaceButtonClickedEvent;

        private List<RaceRecordRow> raceRecordRows;

        private Button startNewRaceButton;

        public override void _Ready()
        {
            base._Ready();

            raceRecordRows = new List<RaceRecordRow>();
            for (var i = 0; i < 5; i++)
                raceRecordRows.Add(GetNode<RaceRecordRow>($"VBoxContainer/RaceRecordRow{i}"));

            startNewRaceButton = GetNode<Button>("StartNewRaceButton");
            startNewRaceButton.Pressed += OnStartNewRaceButtonClicked;
            startNewRaceButton.Visible = false;
        }

        public override void _ExitTree()
        {
            base._ExitTree();

            StartNewRaceButtonClickedEvent = (sender, args) => { };
            raceRecordRows.Clear();
        }

        public void SetupRaceRecordRows(IEnumerable<Core.RaceScene.RaceRecord> raceRecords)
        {
            for (var i = 0; i < raceRecordRows.Count; i++)
            {
                var row = raceRecordRows[i];
                var record = raceRecords.ElementAt(i);

                row.RankLabel.Text = string.Format(TranslationServer.Translate($"PLACE_{record.Rank}"), record.Rank);
                row.LaneLabel.Text = string.Format(TranslationServer.Translate($"LANE_X"), record.Lane);
                row.NameLabel.Text = $"{record.Name}";
            }
        }

        public async void PlayShowAnimation()
        {
            Visible = true;

            await Task.Delay(1000);

            ShowAnimationFinishedEvent(this, EventArgs.Empty);
            ShowAnimationFinishedEvent = (sender, args) => { };
        }

        public void OnStartNewRaceButtonClicked()
        {
            StartNewRaceButtonClickedEvent(this, EventArgs.Empty);
        }

        public void ShowStartNewRaceButton()
        {
            startNewRaceButton.Visible = true;
        }
    }
}
