﻿using Godot;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class RaceRecordRow : Control
    {
        public Label RankLabel;
        public Label LaneLabel;
        public Label NameLabel;

        public override void _Ready()
        {
            base._Ready();

            RankLabel = GetNode<Label>("HBoxContainer/RankLabel");
            LaneLabel = GetNode<Label>("HBoxContainer/LaneLabel");
            NameLabel = GetNode<Label>("HBoxContainer/NameLabel");
        }
    }
}
