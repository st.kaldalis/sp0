﻿using Sample.SP0.Client.Core.RaceScene.View;
using Sample.SP0.Client.GodotWorld.View;
using Godot;
using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class Camera : Camera2D, ICamera
    {
        internal Starter Starter;
        internal IReadOnlyDictionary<int, Node2D> StartFlatforms;
        internal IReadOnlyDictionary<int, Runner> Runners;
        private Tween tween;

        private bool isFollowFrontRunner;

        private Vector2 defaultZoom;

        [Export]
        public float ZoomOutAnimationTime;
        [Export]
        public float ZoomInToStarterAnimation;
        [Export]
        public float ZoomValueOnZoomInToStarterAnimation;
        [Export]
        public float FollowRelativeSpeed;
        [Export]
        public float FollowTargetOffset;

        public override void _Ready()
        {
            base._Ready();

            isFollowFrontRunner = false;
        }

        public override void _Process(double delta)
        {
            base._Process(delta);

            if (isFollowFrontRunner)
            {
                Runner frontRunner = GetFrontRunner();
                if (frontRunner != null)
                {
                    float distance = Position.X - frontRunner.Position.X;
                    float speed = frontRunner.GetSpeed() + FollowRelativeSpeed;
                    speed *= distance > -FollowTargetOffset ? -1 : 1;
                    if (distance < -FollowTargetOffset)
                        speed = frontRunner.GetSpeed();
                    Position = new Vector2(Position.X + speed * (float)delta, Position.Y);
                }
            }
        }

        public void SetDefaultZoom(Godot.Vector2 zoom)
        {
            Zoom = zoom;
            defaultZoom = zoom;
        }

        private Runner GetFrontRunner()
        {
            Runner frontRunner = null;
            float longestDistance = 0.0f;
            foreach (KeyValuePair<int, Runner> entry in Runners)
            {
                int lane = entry.Key;
                Runner runner = entry.Value;
                Node2D startFlatform = StartFlatforms[lane];
                float distance = Math.Abs(entry.Value.GlobalPosition.X - startFlatform.GlobalPosition.X);

                if (longestDistance < distance)
                {
                    frontRunner = runner;
                    longestDistance = distance;
                }
            }

            return frontRunner;
        }

        public void FollowFrontRunner()
        {
            isFollowFrontRunner = true;
        }

        public void PlayZoomInToStarterAnimation()
        {
            Position = Starter.Position;

            var tween = GetNewTween();
            var propertyTweener = tween.TweenProperty(this, "zoom", defaultZoom * ZoomValueOnZoomInToStarterAnimation, ZoomInToStarterAnimation);
            tween.Finished += OnTweenFinished;
        }

        public void PlayZoomOutAnimation()
        {
            var tween = GetNewTween();
            tween.TweenProperty(this, "zoom", defaultZoom, ZoomOutAnimationTime);
            tween.Finished += OnTweenFinished;
        }

        private Tween GetNewTween()
        {
            if (tween != null)
                tween.Kill();
            tween = CreateTween();
            return tween;
        }

        private void OnTweenFinished()
        {
            tween.Kill();
            tween = null;
        }
    }
}
