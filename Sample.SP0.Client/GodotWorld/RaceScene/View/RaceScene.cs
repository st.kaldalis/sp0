using Sample.SP0.Client.Core.RaceScene.View;
using Sample.SP0.Client.GodotWorld.View;
using Sample.SP0.UserDataServer.Protocol;
using Godot;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class RaceScene : Scene, IRaceScene
    {
        public event EventHandler ShowRaceResultBoardAnimationFinishedEvent;
        public event EventHandler ReceivePrizeCoinAnimationFinishedEvent;

        [Export]
        public int LaneCount;
        [Export]
        public float LaneLength;
        public Dictionary<int, Runner> Runners { private set; get; }
        public IReadOnlyDictionary<int, float> FinalLineXPositions { private set; get; }
        public Starter Starter { private set; get; }
        public Camera Camera { private set; get; }

        private Dictionary<int, Node2D> startFlatforms;

        private WindowStack windowStack;
        public RaceResultBoard RaceResultBoard;
        private ReceivePrizeCoinAnimation receivePrizeCoinAnimation;
        public BankruptEndingAnimationBoard BankruptEndingAnimationBoard;

        public override void _Ready()
        {
            base._Ready();

            Starter = GetNode<Starter>("Starter");

            startFlatforms = new Dictionary<int, Node2D>();
            for (var i = 1; i <= LaneCount; i++)
                startFlatforms.Add(i, GetNode<Node2D>($"StartFlatform{i}"));

            Runners = new Dictionary<int, Runner>();

            Camera = GetNode<Camera>("Camera");
            Camera.Starter = Starter;
            Camera.StartFlatforms = startFlatforms;
            Camera.Runners = Runners;

            windowStack = GetNode<WindowStack>("CanvasLayer/WindowStack");
            RaceResultBoard = windowStack.GetNode<RaceResultBoard>("RaceResultBoard");
            receivePrizeCoinAnimation = windowStack.GetNode<ReceivePrizeCoinAnimation>("ReceivePrizeCoinAnimation");
            BankruptEndingAnimationBoard = windowStack.GetNode<BankruptEndingAnimationBoard>("BankruptEndingAnimationBoard");

            Runners = Runners;

            var finalLineXPositions = new Dictionary<int, float>();
            for (var i = 1; i <= LaneCount; i++)
                finalLineXPositions.Add(i, startFlatforms[i].Position.X + LaneLength);
            FinalLineXPositions = finalLineXPositions;

            var background = GetNode<TileMap>("Background");
            var viewportVisibleRectSize = GetViewport().GetVisibleRect().Size;
            Camera.Position = background.Position;
            var zoom = viewportVisibleRectSize.Y / (background.GetUsedRect().Size.Y * background.TileSet.TileSize.Y);
            Camera.SetDefaultZoom(new Godot.Vector2(zoom, zoom));
        }

        public async Task<Runner> PrepareRunner(int lane, RunnerInformation runnerInformation)
        {
            if (!startFlatforms.ContainsKey(lane))
                return null;

            var original = GD.Load<PackedScene>($"res://Assets/Runners/{runnerInformation.RunnerID}.tscn");
            var instance = original.Instantiate();

            await ToSignal(GetTree(), "process_frame");
            var runner = instance as Runner;
            Runners.Add(lane, runner);

            AddChild(runner);

            var startFlatform = startFlatforms[lane];
            runner.Position = startFlatform.Position;
            runner.ZIndex = startFlatform.ZIndex;

            var runnerNameGenerator = new RunnerNameGenerator();
            runner.RunnerName = runnerNameGenerator.GetName(runnerInformation);

            return runner;
        }

        public void PlayShowRaceResultBoardAnimation(IEnumerable<Core.RaceScene.RaceRecord> raceRecords)
        {
            RaceResultBoard.SetupRaceRecordRows(raceRecords);

            RaceResultBoard.ShowAnimationFinishedEvent += (sender, args) =>
            {
                ShowRaceResultBoardAnimationFinishedEvent(this, EventArgs.Empty);
            };
            RaceResultBoard.PlayShowAnimation();

            windowStack.OnWindowOpened(RaceResultBoard);
        }

        public void PlayReceivePrizeCoinAnimation(int prizeCoin)
        {
            receivePrizeCoinAnimation.SetPrizeCoin(prizeCoin);

            receivePrizeCoinAnimation.FinishedEvent += (sender, args) =>
            {
                ReceivePrizeCoinAnimationFinishedEvent(this, EventArgs.Empty);

                windowStack.OnWindowClosed(receivePrizeCoinAnimation);
            };
            receivePrizeCoinAnimation.Play();

            windowStack.OnWindowOpened(receivePrizeCoinAnimation);
        }

        public void ShowStartNewRaceButton()
        {
            RaceResultBoard.ShowStartNewRaceButton();
        }

        public void PlayBankruptEndingAnimation()
        {
            BankruptEndingAnimationBoard.Visible = true;
            BankruptEndingAnimationBoard.PlayAnimation();

            windowStack.OnWindowOpened(BankruptEndingAnimationBoard);
        }
    }
}
