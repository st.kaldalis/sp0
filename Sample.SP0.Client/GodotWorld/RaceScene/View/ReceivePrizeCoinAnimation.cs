﻿using Godot;
using System;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class ReceivePrizeCoinAnimation : Control
    {
        public event EventHandler FinishedEvent;

        private Label prizeCoinLabel;
        private Control closeButtonPanel;

        public override void _Ready()
        {
            base._Ready();

            prizeCoinLabel = GetNode<Label>("PrizeCoinLabel");

            closeButtonPanel = GetNode<Control>("CloseButtonPanel");
            var btn = closeButtonPanel.GetNode<Button>("Button");
            btn.Pressed += OnCloseButtonPressed;
        }

        private void OnCloseButtonPressed()
        {
            FinishedEvent(this, EventArgs.Empty);
            FinishedEvent = (sender, args) => { };

            Visible = false;
        }

        public void SetPrizeCoin(int prizeCoin)
        {
            prizeCoinLabel.Text = $"+{prizeCoin}";
        }

        public async void Play()
        {
            Visible = true;
            closeButtonPanel.Visible = false;

            await Task.Delay(1000);

            closeButtonPanel.Visible = true;
        }
    }
}
