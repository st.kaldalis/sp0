﻿using Sample.SP0.Client.Core.RaceScene.View;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class Starter : Node2D, IStarter
    {
        public event EventHandler StartingGunFiredEvent;

        private AnimationPlayer animationPlayer;

        public override void _Ready()
        {
            base._Ready();

            animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
        }

        public void Wait()
        {
            animationPlayer.CurrentAnimation = "Wait";
        }

        public void FireStartingGun()
        {
            animationPlayer.CurrentAnimation = "Fire";
        }

        public void OnStartingGunFired()
        {
            StartingGunFiredEvent(this, EventArgs.Empty);
        }

        public void OnFireFinished()
        {
            Wait();
        }
    }
}
