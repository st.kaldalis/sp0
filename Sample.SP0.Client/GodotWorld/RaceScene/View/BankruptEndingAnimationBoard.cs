﻿using Sample.SP0.Client.Core.RaceScene.View;
using Godot;
using System;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld.RaceScene.View
{
    public partial class BankruptEndingAnimationBoard : Control, IBankruptEndingAnimationBoard
    {
        public event EventHandler GoToTitleButtonClickedEvent;
        public event EventHandler AnimationFinishedEvent;

        private Control goToTitleButtonPanel;

        public override void _Ready()
        {
            base._Ready();

            goToTitleButtonPanel = GetNode<Control>("GoToTitleButtonPanel");
            var btn = goToTitleButtonPanel.GetNode<Button>("Button");
            btn.Pressed += OnGoToTitleButtonClicked;
        }

        private void OnGoToTitleButtonClicked()
        {
            GoToTitleButtonClickedEvent(this, EventArgs.Empty);
        }

        public override void _ExitTree()
        {
            base._ExitTree();

            GoToTitleButtonClickedEvent = (sender, args) => { };
            AnimationFinishedEvent = (sender, args) => { };
        }

        public async void PlayAnimation()
        {
            await Task.Delay(1000);
            AnimationFinishedEvent(this, EventArgs.Empty);
        }

        public void ShowGoToTitleButton()
        {
            goToTitleButtonPanel.Visible = true;
        }
    }
}
