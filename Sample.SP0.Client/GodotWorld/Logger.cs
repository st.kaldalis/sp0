﻿using Godot;

namespace Sample.SP0.Client.GodotWorld
{
    public class Logger : ILogger
    {
        public void Fatal(string message)
        {
            GD.PrintErr(message);
        }
    }
}