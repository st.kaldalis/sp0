using Sample.SP0.Client.Core;
using Sample.SP0.Client.Core.RaceScene;
using Sample.SP0.UserDataServer.Protocol;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld
{
    public class DummyServerApi : IServerApi
    {
        private readonly GameData gameData;
        private readonly Random random;
        private readonly string userDataFileName;
        private UserData userData;
        private List<Tuple<int, RunnerInformation>> runnerInformations;

        public DummyServerApi(GameData gameData)
        {
            this.gameData = gameData;
            random = new Random();
            userDataFileName = "user://userData.txt";
            userData = Load();
        }

        public Task<GetNewRaceResult> GetNewRace()
        {
            var runnerIDs = gameData.GetRunnerIDs();
            var runnerInformations = new List<Tuple<int, RunnerInformation>>();
            for (var i = 1; i <= gameData.GetLaneCount(); i++)
            {
                var runnerInformation = new RunnerInformation();
                runnerInformation.RunnerID = runnerIDs.ElementAt(random.Next(runnerIDs.Count()));
                runnerInformation.Odds = (float)Math.Round(random.NextDouble(), 1) + random.Next(1, 10);
                var nameAParts = gameData.GetRunnerNameAPartTextIDs();
                runnerInformation.NameAPartTextID = nameAParts.ElementAt(random.Next(0, nameAParts.Count()));
                var nameBParts = gameData.GetRunnerNameBPartTextIDs();
                runnerInformation.NameBPartTextID = nameBParts.ElementAt(random.Next(0, nameBParts.Count()));
                runnerInformation.Weight = GenerateWeight();
                runnerInformation.Strength = GenerateStrength();
                runnerInformation.Condition = GenerateCondition();
                runnerInformation.Stamina = GenerateStamina();
                runnerInformation.Concentration = GenerateConcentration();

                runnerInformations.Add(new Tuple<int, RunnerInformation>(i, runnerInformation));
            }

            this.runnerInformations = runnerInformations;

            var result = new GetNewRaceResult
            {
                IsSuccess = true,
                RunnerInformations = runnerInformations
            };

            return Task.FromResult(result);
        }

        private int GenerateConcentration()
        {
            var concentrationRange = gameData.GetConcentrationRange();
            return random.Next(concentrationRange.Item1, concentrationRange.Item2);
        }

        private int GenerateStamina()
        {
            var staminaRange = gameData.GetStaminaRange();
            return random.Next(staminaRange.Item1, staminaRange.Item2);
        }

        private int GenerateCondition()
        {
            var conditionRange = gameData.GetConditionRange();
            return random.Next(conditionRange.Item1, conditionRange.Item2);
        }

        private int GenerateStrength()
        {
            var strengthRange = gameData.GetStrengthRange();
            return random.Next(strengthRange.Item1, strengthRange.Item2);
        }

        private int GenerateWeight()
        {
            var weightRange = gameData.GetWeightRange();
            return random.Next(weightRange.Item1, weightRange.Item2);
        }

        public Task<GetWalletResult> GetWallet()
        {
            var result = new GetWalletResult();
            result.IsSuccess = true;
            result.Coin = userData.Wallet.Coin;

            return Task.FromResult(result);
        }

        public Task<GetRaceRecordsResult> GetRaceRecords(int bettedLane, int bettedCoin)
        {
            var result = new GetRaceRecordsResult();
            result.IsSuccess = true;
            var raceRecords = new List<UserDataServer.Protocol.RaceRecord>();
            result.RaceRecords = raceRecords;
            result.IsWin = false;
            result.Prize = 0;

            float laneLength = 10000.0f;

            var remainTrackLengths = new List<float>();
            var lastSpeeds = new List<float>();
            var runningTimes = new List<float>();
            var sectionRecordsList = new List<List<SectionRecord>>();
            var random = new Random();
            for (var i = 1; i <= gameData.GetLaneCount(); i++)
            {
                var raceRecord = new UserDataServer.Protocol.RaceRecord();
                raceRecord.Lane = i;
                raceRecord.ResponseTime = (float)random.Next(67, 73) * 0.001f;
                var sectionRecords = new List<SectionRecord>();
                raceRecord.SectionRecords = sectionRecords;
                raceRecord.RunningTime = raceRecord.ResponseTime;
                raceRecords.Add(raceRecord);

                remainTrackLengths.Add(laneLength);
                lastSpeeds.Add(0.0f);
                runningTimes.Add(raceRecord.RunningTime);
                sectionRecordsList.Add(sectionRecords);
            }

            var runnerIndexes = new List<int>();
            while (true)
            {
                for (var i = 0; i < remainTrackLengths.Count; i++)
                {
                    if (remainTrackLengths[i] > 0)
                        runnerIndexes.Add(i);
                }

                if (runnerIndexes.Count == 0)
                    break;

                var lastOne = -1;
                var firstOne = -1;
                var max = -1.0f;
                var min = laneLength;
                foreach (var runnerIndex in runnerIndexes)
                {
                    if (max < remainTrackLengths[runnerIndex])
                    {
                        max = remainTrackLengths[runnerIndex];
                        lastOne = runnerIndex;
                    }

                    if (remainTrackLengths[runnerIndex] <= min)
                    {
                        min = remainTrackLengths[runnerIndex];
                        firstOne = runnerIndex;
                    }
                }

                foreach (var runnerIndex in runnerIndexes)
                {
                    var lastSpeed = lastSpeeds[runnerIndex];
                    var remainTrackLength = remainTrackLengths[runnerIndex];
                    var raceRecord = raceRecords[runnerIndex];
                    var sectionRecords = sectionRecordsList[runnerIndex];

                    var time = 0.05f + random.Next(0, 20) * 0.001f;

                    var initialSpeed = lastSpeed;
                    var acceleration = 0.0f;
                    if (firstOne != lastOne && firstOne != runnerIndex)
                        acceleration = random.Next(0, 450) + 200.0f;
                    if (firstOne != lastOne && firstOne == runnerIndex)
                        acceleration = random.Next(0, 50) + 100;

                    var oldRemainTrackLength = remainTrackLength;
                    remainTrackLength -= initialSpeed * time + 0.5f * acceleration * time * time;
                    remainTrackLengths[runnerIndex] = remainTrackLength < 0 ? 0 : remainTrackLength;

                    lastSpeed = initialSpeed + acceleration * time;
                    lastSpeeds[runnerIndex] = lastSpeed;

                    var sectionRecord = new SectionRecord();
                    sectionRecord.InitialSpeed = initialSpeed;
                    sectionRecord.Acceleration = acceleration;
                    sectionRecord.StartTime = raceRecord.RunningTime;
                    sectionRecord.EndTime = sectionRecord.StartTime + time;
                    sectionRecords.Add(sectionRecord);

                    if (remainTrackLength < 0)
                    {
                        var a = 0.5f * acceleration;
                        var b = initialSpeed;
                        var c = -oldRemainTrackLength;
                        if (a > 0)
                            raceRecord.RunningTime += (-b + (float)Math.Sqrt(b * b - 4 * a * c)) / (2 * a);
                        else
                            raceRecord.RunningTime += -c / b;
                    }
                    else
                    {
                        raceRecord.RunningTime += time;
                    }

                    raceRecords[runnerIndex] = raceRecord;
                }

                runnerIndexes.Clear();
            }

            userData.Wallet.Coin -= bettedCoin;

            var winnerRaceRecord = raceRecords.OrderBy(item => item.RunningTime).FirstOrDefault();
            if (winnerRaceRecord.Lane == bettedLane)
            {
                var runnerInfo = runnerInformations.Where(entry => entry.Item1 == winnerRaceRecord.Lane).Select(entry => entry.Item2).FirstOrDefault();
                var prize = (int)Math.Floor(bettedCoin * runnerInfo.Odds);
                userData.Wallet.Coin += prize;

                result.IsWin = true;
                result.Prize = prize;
            }

            result.Coin = userData.Wallet.Coin;

            Save(userData);

            return Task.FromResult(result);
        }

        public Task RemoveUser()
        {
            var dir = DirAccess.Open("user://");
            dir.Remove(userDataFileName);

            userData = CreateUserData();

            return Task.CompletedTask;
        }

        private void Save(UserData userData)
        {
            using var userDataFile = FileAccess.Open(userDataFileName, FileAccess.ModeFlags.Write);
            userDataFile.StoreLine(Serialize(userData));
        }

        private UserData Load()
        {
            var userData = CreateUserData();
            if (!FileAccess.FileExists(userDataFileName))
                return userData;

            using var userDataFile = FileAccess.Open(userDataFileName, FileAccess.ModeFlags.Read);
            userData = Deserialize(userDataFile.GetAsText());

            return userData;
        }

        private UserData Deserialize(string serializedData)
        {
            var userData = CreateUserData();
            var data = new Godot.Collections.Dictionary<string, Variant>((Godot.Collections.Dictionary)Json.ParseString(serializedData));
            userData.Wallet.Coin = data["Coin"].AsInt32();

            return userData;
        }

        private string Serialize(UserData userData)
        {
            var serializedData = new Godot.Collections.Dictionary<string, Variant>();
            serializedData["Coin"] = userData.Wallet.Coin;

            return Json.Stringify(serializedData);
        }

        private UserData CreateUserData()
        {
            var wallet = new Wallet();
            wallet.Coin = gameData.GetInitialCoin();

            var userData = new UserData();
            userData.Wallet = wallet;

            return userData;
        }

        private struct Wallet
        {
            public int Coin;
        }

        private struct UserData
        {
            public Wallet Wallet;
        }
    }
}
