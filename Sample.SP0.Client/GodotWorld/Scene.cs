﻿using Godot;

namespace Sample.SP0.Client.GodotWorld
{
    public partial class Scene : Node, IScene
    {
        public void Destroy()
        {
            QueueFree();
        }
    }
}
