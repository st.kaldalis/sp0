﻿using Sample.SP0.UserDataServer.Protocol;
using Godot;

namespace Sample.SP0.Client.GodotWorld
{
    public class RunnerNameGenerator
    {
        public string GetName(RunnerInformation runnerInformation)
        {
            var nameAPart = TranslationServer.Translate(runnerInformation.NameAPartTextID);
            var nameBPart = TranslationServer.Translate(runnerInformation.NameBPartTextID);
            if (nameAPart == runnerInformation.NameAPartTextID)
                return $"{nameBPart}";
            return $"{nameAPart} {nameBPart}";
        }
    }
}
