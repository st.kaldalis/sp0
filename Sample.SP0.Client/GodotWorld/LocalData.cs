﻿using Godot;

namespace Sample.SP0.Client.GodotWorld
{
    public class LocalData : Core.LocalData
    {
        private readonly string localDataFileName;

        public LocalData()
        {
            Locale = OS.GetLocaleLanguage();

            localDataFileName = "user://localData.txt";
        }

        public void Load()
        {
            if (!FileAccess.FileExists(localDataFileName))
                return;

            var localDataFile = FileAccess.Open(localDataFileName, FileAccess.ModeFlags.Read);

            var dic = (Godot.Collections.Dictionary)Json.ParseString(localDataFile.GetLine());
            var serializedData = new Godot.Collections.Dictionary<string, Variant>(dic);
            Locale = serializedData["Language"].AsString();
            LastAdsPlayedTime = serializedData.ContainsKey("LastAdsPlayedTime") ? serializedData["LastAdsPlayedTime"].AsInt64() : 0;

            localDataFile.Close();
        }


        public override void SetLastAdsPlayedTime(long lastPlayedTime)
        {
            LastAdsPlayedTime = lastPlayedTime;

            Save();
        }

        private void Save()
        {
            var serializedData = new Godot.Collections.Dictionary<string, Variant>();
            serializedData["Language"] = Locale;
            serializedData["LastAdsPlayedTime"] = LastAdsPlayedTime;

            using var userDataFile = FileAccess.Open(localDataFileName, FileAccess.ModeFlags.Write);
            userDataFile.StoreLine(Json.Stringify(serializedData));
        }

        public override void SetLocale(string locale)
        {
            Locale = locale;

            Save();
        }
    }
}
