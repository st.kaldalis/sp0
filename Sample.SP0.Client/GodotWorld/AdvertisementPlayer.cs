﻿using Sample.SP0.Client.Core;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld
{
    public partial class AdvertisementPlayer : Node, IAdvertisementPlayer
    {
        public event EventHandler InterstitialAdvertisementClosedEvent;

        private Node advertisementApi;

        private CanvasLayer adsBackground;

        public bool IsInterstitialAdvertisementReady { get; private set; }

        public override void _Ready()
        {
            IsInterstitialAdvertisementReady = false;

            adsBackground = GetNode<CanvasLayer>("CanvasLayer");

            if (OS.GetName() == "Android" || OS.GetName() == "iOS")
            {
                advertisementApi = GetTree().Root.GetNode("Node2D/AdvertisementApi");

                advertisementApi.Connect("initialize_finished", new Callable(this, MethodName.OnInitializeFinished));
                advertisementApi.Connect("load_interstitial_advertisement_finished", new Callable(this, MethodName.OnLoadInterstitialAdvertisementFinished));
                advertisementApi.Connect("interstitial_advertisement_dismissed", new Callable(this, MethodName.OnInterstitialAdvertisementDismissed));

                advertisementApi.Call("initialize");
            }
        }

        private void OnInitializeFinished()
        {
            LoadInterstitialAdvertisement();
        }

        private void LoadInterstitialAdvertisement()
        {
            CallAdApiFunc("load_interstitial_advertisement");
        }

        private void OnLoadInterstitialAdvertisementFinished(bool isLoadSuccess)
        {
            IsInterstitialAdvertisementReady = isLoadSuccess;
        }

        public void PlayInterstitialAdvertisement()
        {
            adsBackground.Visible = true;

            CallAdApiFunc("show_interstitial_advertisement");

            IsInterstitialAdvertisementReady = false;
        }

        private void OnInterstitialAdvertisementDismissed()
        {
            adsBackground.Visible = false;

            LoadInterstitialAdvertisement();

            InterstitialAdvertisementClosedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void CallAdApiFunc(string funcName)
        {
            advertisementApi?.Call(funcName);
        }
    }

}
