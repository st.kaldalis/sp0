using Sample.SP0.Client.Core;
using Godot;

namespace Sample.SP0.Client.GodotWorld
{
	public partial class Starter : Node
	{
		// Called when the node enters the scene tree for the first time.
		public override void _Ready()
		{
			var screen = GetTree().Root.GetNode<Node2D>("Node2D");
			var loadingBlind = GetTree().Root.GetNode<Control>("Node2D/CanvasLayer/LoadingBlind");
			var sceneSetter = GetTree().Root.GetNode<SceneSetter>("Node2D/SceneSetter");
			var advertisementPlayer = GetTree().Root.GetNode<AdvertisementPlayer>("Node2D/AdvertisementPlayer");
			var time = GetTree().Root.GetNode<Time>("Node2D/Time");
			var loop = GetTree().Root.GetNode<Loop>("Node2D/Loop");
			var logger = new Logger();
			var gameData = new GameData();
			var dummyServerApi = new DummyServerApi(gameData);
			var localData = new LocalData();
			localData.Load();

			TranslationServer.SetLocale(localData.Locale);

			sceneSetter.Setup(screen, loadingBlind, dummyServerApi, localData,
				logger, gameData, advertisementPlayer, loop, time);
			sceneSetter.SetTitleScene();

			QueueFree();
		}
	}
}
