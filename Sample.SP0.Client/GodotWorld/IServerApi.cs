﻿using Sample.SP0.UserDataServer.Protocol;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld
{
    public interface IServerApi
    {
        Task RemoveUser();
        Task<GetWalletResult> GetWallet();
        Task<GetNewRaceResult> GetNewRace();
        Task<GetRaceRecordsResult> GetRaceRecords(int bettedLane, int bettedCoin);
    }
}