﻿using Sample.SP0.Client.Core;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld
{
    public partial class Time : Node, ITime
    {
        private float delta;
        private long currentTime;

        public long GetCurrentTime()
        {
            return currentTime;
        }

        public float GetDeltaTime()
        {
            return delta;
        }

        public override void _Process(double delta)
        {
            base._Process(delta);

            this.delta = (float)delta;
            currentTime = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeMilliseconds();
        }
    }
}
