using Sample.SP0.Client.Core.BettingScene.View;
using Sample.SP0.Client.GodotWorld.View;
using Sample.SP0.UserDataServer.Protocol;
using Godot;
using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.GodotWorld.BettingScene.View
{
    public partial class BettingScene : Scene, IBettingScene
    {
        public event EventHandler StartRaceButtonClickedEvent;
        public event EventHandler NextButtonClickedEvent;
        public event EventHandler PrevButtonClickedEvent;
        public event EventHandler BettingButtonClickedEvent;

        private Label coinLabel;
        private RunnerInformationBoard runnerInformationBoard;
        private Button startRaceButton;
        private TextureButton nextButton;
        private TextureButton prevButton;
        private Button bettingButton;
        private Control bettedCoinContainer;
        private Label bettedCoinLabel;

        private WindowStack windowStack;
        private BettingWindow bettingWindow;
        private MessageBox messageBox;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            coinLabel = GetNode<Label>("CanvasLayer/Control/CoinContainer/HBoxContainer/CoinLabel");
            runnerInformationBoard = GetNode<RunnerInformationBoard>("CanvasLayer/Control/RunnerInformationBoard");
            startRaceButton = GetNode<Button>("CanvasLayer/Control/StartRaceButton");
            nextButton = GetNode<TextureButton>("CanvasLayer/Control/NextButton");
            prevButton = GetNode<TextureButton>("CanvasLayer/Control/PrevButton");
            bettingButton = GetNode<Button>("CanvasLayer/Control/BettingButton");
            windowStack = GetNode<WindowStack>("CanvasLayer/WindowStack");
            bettingWindow = GetNode<BettingWindow>("CanvasLayer/WindowStack/BettingWindow");
            messageBox = GetNode<MessageBox>("CanvasLayer/WindowStack/MessageBox");
            bettedCoinContainer = GetNode<Control>("CanvasLayer/Control/BettedCoinContainer");
            bettedCoinLabel = GetNode<Label>("CanvasLayer/Control/BettedCoinContainer/Label");

            startRaceButton.Pressed += OnStartRaceButtonPressed;
            nextButton.Pressed += OnNextButtonPressed;
            prevButton.Pressed += OnPrevButtonPressed;
            bettingButton.Pressed += OnBettingButtonPressed;
        }

        private void OnStartRaceButtonPressed()
        {
            StartRaceButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void OnNextButtonPressed()
        {
            NextButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void OnPrevButtonPressed()
        {
            PrevButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void OnBettingButtonPressed()
        {
            BettingButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                StartRaceButtonClickedEvent = (sender, args) => { };
                NextButtonClickedEvent = (sender, args) => { };
                PrevButtonClickedEvent = (sender, args) => { };
                BettingButtonClickedEvent = (sender, args) => { };
            }
        }

        public void SetCoin(int coin)
        {
            coinLabel.Text = $"{coin}";
        }

        public void SetRunnerInformation(int lane, RunnerInformation runnerInformation)
        {
            runnerInformationBoard.SetRunnerInformation(lane, runnerInformation);
        }

        public IBettingWindow OpenBettingWindow()
        {
            bettingWindow.Visible = true;
            bettingWindow.ClosedEvent += (sender, args) => { windowStack.OnWindowClosed(bettingWindow); };

            windowStack.OnWindowOpened(bettingWindow);

            return bettingWindow;
        }

        public void SetVisibleBettedCoinContainer(bool isVisible)
        {
            bettedCoinContainer.Visible = isVisible;
        }

        public void SetBettedCoin(int bettedCoin)
        {
            bettedCoinLabel.Text = $"{bettedCoin}";
        }

        public IMessageBox OpenMessageBox()
        {
            messageBox.Visible = true;
            messageBox.ClosedEvent += (sender, args) => { windowStack.OnWindowClosed(messageBox); };

            windowStack.OnWindowOpened(messageBox);

            return messageBox;
        }
    }
}
