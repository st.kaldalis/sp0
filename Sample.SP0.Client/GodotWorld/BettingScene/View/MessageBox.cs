﻿using Sample.SP0.Client.Core.BettingScene.View;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.BettingScene.View
{
    public partial class MessageBox : Control, IMessageBox
    {
        public event EventHandler ClosedEvent;
        public event EventHandler OKButtonClickedEvent;

        private Label label;

        public override void _Ready()
        {
            base._Ready();

            var okBtn = GetNode<Button>("Button");
            okBtn.Pressed += OnOKButtonPressed;

            label = GetNode<Label>("Background/Label");
        }

        private void OnOKButtonPressed()
        {
            OKButtonClickedEvent(this, EventArgs.Empty);
        }

        public void Close()
        {
            Visible = false;

            ClosedEvent(this, EventArgs.Empty);

            ClosedEvent = (sender, args) => { };
            OKButtonClickedEvent = (sender, args) => { };
        }

        public void SetMessage(string textID)
        {
            label.Text = TranslationServer.Translate(textID);
        }
    }
}
