using Sample.SP0.Client.GodotWorld.View;
using Sample.SP0.UserDataServer.Protocol;
using Godot;

namespace Sample.SP0.Client.GodotWorld.BettingScene.View
{
    public partial class RunnerInformationBoard : Node
    {
        private Label laneLabel;
        private Label nameLabel;
        private Label weightLabel;
        private Label strengthLabel;
        private Label conditionLabel;
        private Label staminaLabel;
        private Label concentrationLabel;
        private Label oddsLabel;
        private Node2D runnerFlatformNode;
        private Runner runner;

        public override void _Ready()
        {
            laneLabel = GetNode<Label>("LanePanel/Label");
            nameLabel = GetNode<Label>("NameLabel");
            weightLabel = GetNode<Label>("StatContainerB/WeightContainer/ValueLabel");
            strengthLabel = GetNode<Label>("StatContainerB/StrengthContainer/ValueLabel");
            conditionLabel = GetNode<Label>("StatContainerA/ConditionContainer/ValueLabel");
            staminaLabel = GetNode<Label>("StatContainerA/StaminaContainer/ValueLabel");
            concentrationLabel = GetNode<Label>("StatContainerA/ConcentrationContainer/ValueLabel");
            oddsLabel = GetNode<Label>("OddsLabel");
            runnerFlatformNode = GetNode<Node2D>("Control/RunnerFlatform");
        }

        public async void SetRunnerInformation(int lane, RunnerInformation runnerInformation)
        {
            laneLabel.Text = string.Format(TranslationServer.Translate($"LANE_X"), lane);

            var runnerNameGenerator = new RunnerNameGenerator();
            nameLabel.Text = runnerNameGenerator.GetName(runnerInformation);

            conditionLabel.Text = $"{runnerInformation.Condition}";
            weightLabel.Text = $"{runnerInformation.Weight}";
            staminaLabel.Text = $"{runnerInformation.Stamina}";
            strengthLabel.Text = $"{runnerInformation.Strength}";
            concentrationLabel.Text = $"{runnerInformation.Concentration}";
            oddsLabel.Text = string.Format(TranslationServer.Translate($"ODDS_X"), runnerInformation.Odds);

            if (runner != null)
            {
                runner.QueueFree();
                runner = null;
            }

            var original = GD.Load<PackedScene>($"res://Assets/Runners/{runnerInformation.RunnerID}.tscn");
            var instance = original.Instantiate();
            await ToSignal(GetTree(), "process_frame");

            runner = instance as Runner;
            runnerFlatformNode.AddChild(runner);

            runner.Wait();
        }
    }
}
