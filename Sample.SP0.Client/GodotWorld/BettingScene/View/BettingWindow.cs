using Sample.SP0.Client.Core.BettingScene.View;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.BettingScene.View
{
    public partial class BettingWindow : Control, IBettingWindow
    {
        public event EventHandler ClosedEvent;
        public event EventHandler CancelButtonClickedEvent;
        public event EventHandler BettingButtonClickedEvent;

        private LineEdit bettedCoinLineEdit;
        private SpinBox bettedCoinSpinBox;
        private Label bettedCoinLabel;
        private int bettedCoin;

        public override void _Ready()
        {
            base._Ready();

            var cancelBtn = GetNode<Button>("CancelButton");
            var bettingBtn = GetNode<Button>("BettingButton");
            bettedCoinSpinBox = GetNode<SpinBox>("SpinBox");

            bettedCoinLabel = GetNode<Label>("BettedCoinPanel/Label");
            bettedCoinLineEdit = bettedCoinSpinBox.GetLineEdit();

            cancelBtn.Pressed += OnCancelButtonClicked;
            bettingBtn.Pressed += OnBettingButtonClicked;
            bettedCoinSpinBox.GetLineEdit().TextChanged += OnBettedCoinLineEditTextChanged;

            ClearEventHandlers();
        }

        private void OnBettedCoinLineEditTextChanged(string text)
        {
            if (string.IsNullOrEmpty(text))
                bettedCoin = 0;

            if (int.TryParse(text, out int newBettedCoin))
                bettedCoin = newBettedCoin;

            bettedCoinLineEdit.Text = $"{bettedCoin}";
            bettedCoinLabel.Text = $"{bettedCoin}";

            bettedCoinLineEdit.CaretColumn = bettedCoinLineEdit.Text.Length;
        }

        public void OnCancelButtonClicked()
        {
            CancelButtonClickedEvent(this, EventArgs.Empty);
        }

        public void OnBettingButtonClicked()
        {
            BettingButtonClickedEvent(this, EventArgs.Empty);
        }

        public void Close()
        {
            Visible = false;

            ClosedEvent(this, EventArgs.Empty);

            bettedCoinSpinBox.SetValueNoSignal(0);

            ClearEventHandlers();
        }

        private void ClearEventHandlers()
        {
            ClosedEvent = (sender, args) => { };
            CancelButtonClickedEvent = (sender, args) => { };
            BettingButtonClickedEvent = (sender, args) => { };
        }

        public void SetBettedCoin(int value)
        {
            bettedCoin = value;
            bettedCoinLabel.Text = $"{value}";
            bettedCoinSpinBox.SetValueNoSignal(value);
        }

        public int GetBettedCoin()
        {
            return Convert.ToInt32(bettedCoinLabel.Text);
        }
    }
}
