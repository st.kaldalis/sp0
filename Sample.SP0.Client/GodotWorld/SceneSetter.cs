﻿using Sample.SP0.Client.Core;
using Sample.SP0.Client.Core.BettingScene;
using Sample.SP0.Client.Core.RaceScene;
using Sample.SP0.Client.Core.TitleScene;
using Sample.SP0.Client.GodotWorld.RaceScene;
using Sample.SP0.Client.GodotWorld.TitleScene;
using Sample.SP0.UserDataServer.Protocol;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld
{
    public partial class SceneSetter : Node, ISceneSetter
    {
        private Node2D screen;
        private Control loadingBlind;
        private IServerApi serverApi;
        private ILogger logger;
        private GameData gameData;
        private IScene lastScene;
        private IAdvertisementPlayer advertisementPlayer;
        private Loop loop;
        private Time time;
        private LocalData localData;
        private UserData userData;

        public void Setup(Node2D screen, Control loadingBlind, IServerApi serverApi, LocalData localData,
            ILogger logger, GameData gameData, IAdvertisementPlayer advertisementPlayer, Loop loop, Time time)
        {
            this.gameData = gameData;
            this.screen = screen;
            this.loadingBlind = loadingBlind;
            this.serverApi = serverApi;
            this.localData = localData;
            this.logger = logger;
            this.advertisementPlayer = advertisementPlayer;
            this.loop = loop;
            this.time = time;
            userData = new UserData(serverApi, logger);
        }

        public async void SetTitleScene()
        {
            await ShowLoadingBlind();

            await DestroyLastScene();

            var instance = await AttachNewScene("res://Assets/TitleScene.tscn");
            var titleScene = instance as TitleScene.View.TitleScene;

            var ctrl = new TitleSceneController(this, titleScene, localData, new TextSet());
            titleScene.StartButtonClickedEvent += ctrl.OnClickStartButton;
            titleScene.LocaleButtonClickedEvent += ctrl.OnClickLocaleButton;

            var scStartedEvtHdlr = new SceneStartedEventHandler(titleScene, userData);
            scStartedEvtHdlr.OnSceneStartedEvent(this, EventArgs.Empty);

            await HideLoadingBlind();
        }

        public async void SetBettingScene()
        {
            await ShowLoadingBlind();

            await DestroyLastScene();

            var getNewRaceResult = await serverApi.GetNewRace();
            if (!getNewRaceResult.IsSuccess)
            {
                logger.Fatal("Failed to get new race result.");
                return;
            }

            var instance = await AttachNewScene("res://Assets/BettingScene.tscn");
            var scene = instance as BettingScene.View.BettingScene;

            var runnerInformations = getNewRaceResult.RunnerInformations;
            scene.SetCoin(userData.Wallet.Coin);
            var entry = runnerInformations.First();
            scene.SetRunnerInformation(entry.Item1, entry.Item2);

            var ctrl = new BettingSceneController(this, runnerInformations, gameData, 1, scene, advertisementPlayer, userData, localData, time);
            scene.StartRaceButtonClickedEvent += ctrl.OnClickStartRaceButton;
            scene.NextButtonClickedEvent += ctrl.OnClickNextButton;
            scene.PrevButtonClickedEvent += ctrl.OnClickPrevButton;
            scene.BettingButtonClickedEvent += ctrl.OnClickBettingButton;

            await HideLoadingBlind();
        }

        public async void SetRaceScene(IEnumerable<Tuple<int, RunnerInformation>> runnerInformations, int bettedLane, int bettedCoin)
        {
            await ShowLoadingBlind();

            await DestroyLastScene();

            var getRaceRecordsResult = await serverApi.GetRaceRecords(bettedLane, bettedCoin);
            if (!getRaceRecordsResult.IsSuccess)
            {
                logger.Fatal("Failed to get race records result.");
                return;
            }

            var instance = await AttachNewScene("res://Assets/RaceScene.tscn");
            var scene = instance as RaceScene.View.RaceScene;

            var raceRecords = getRaceRecordsResult.RaceRecords;
            await SetupRunnersAsync(scene, runnerInformations, raceRecords);

            var starter = scene.Starter;
            if (starter == null)
                return;
            starter.Wait();

            var camera = scene.Camera;
            if (camera == null)
                return;

            var raceResutlBoard = scene.RaceResultBoard;
            if (raceResutlBoard == null)
                return;

            await HideLoadingBlind();

            raceResutlBoard.StartNewRaceButtonClickedEvent += (sender, args) => { SetBettingScene(); };

            var prizeBooth = new PrizeBooth(getRaceRecordsResult.Prize);

            var raceFinishedEvtHdlr = new RaceFinishedEventHandler(scene, prizeBooth, bettedLane, userData);

            var raceJudge = new RaceScene.RaceJudge(scene.Runners, scene.FinalLineXPositions, raceRecords);
            raceJudge.RaceFinishedEvent += (sender, args) =>
            {
                loop.Remove(raceJudge);
                raceFinishedEvtHdlr.OnRaceFinishedEvent(sender, args);
            };
            loop.Add(raceJudge);

            var bankruptEndingAnimBrd = scene.BankruptEndingAnimationBoard;
            var bankruptEndingAnimBrdCtrl = new BankruptEndingAnimationBoardController(bankruptEndingAnimBrd, this);
            bankruptEndingAnimBrd.AnimationFinishedEvent += bankruptEndingAnimBrdCtrl.OnAnimationFinishedEvent;
            bankruptEndingAnimBrd.GoToTitleButtonClickedEvent += bankruptEndingAnimBrdCtrl.OnGoToTitleButtonClickedEvent;

            camera.PlayZoomInToStarterAnimation();

            var startDelayTimer = new Core.Timer(time, loop);
            var evtHdlr = new StartDelayFinishedAlarmEventHandler(starter, camera);
            startDelayTimer.AlarmEvent += evtHdlr.OnStartDelayFinishedAlarmEvent;

            startDelayTimer.Set(gameData.GetStartDelay());
            startDelayTimer.Start();
        }

        private async Task SetupRunnersAsync(RaceScene.View.RaceScene scene, IEnumerable<Tuple<int, RunnerInformation>> runnerInformations, IEnumerable<UserDataServer.Protocol.RaceRecord> raceRecords)
        {
            var starter = scene.Starter;
            if (starter == null)
                return;

            foreach (var entry in runnerInformations)
            {
                var lane = entry.Item1;
                var runnerInfo = entry.Item2;

                if (!raceRecords.Any(record => record.Lane == lane))
                {
                    logger.Fatal($"Failed to find runner record. {lane}");
                    return;
                }

                var raceRecord = raceRecords.First(record => record.Lane == lane);
                var runner = await scene.PrepareRunner(lane, runnerInfo);
                if (runner == null)
                    return;

                runner.Wait();

                var runnerCtrl = new RunnerController(runner, raceRecord, time);
                starter.StartingGunFiredEvent += runnerCtrl.OnStartingGunFiredEvent;

                loop.Add(runnerCtrl);
                runner.DestroyedEvent += (sender, args) =>
                {
                    loop.Remove(runnerCtrl);
                };
            }
        }

        private async Task ShowLoadingBlind()
        {
            loadingBlind.Visible = true;

            await ToSignal(GetTree(), "process_frame");
        }

        private async Task HideLoadingBlind()
        {
            await ToSignal(GetTree(), "process_frame");

            loadingBlind.Visible = false;
        }

        private async Task<Node> AttachNewScene(string path)
        {
            var scene = GD.Load<PackedScene>(path);
            var instance = scene.Instantiate();
            screen.CallDeferred("add_child", instance);
            screen.CallDeferred("move_child", instance, 0);

            lastScene = instance as IScene;

            await ToSignal(GetTree(), "process_frame");

            return instance;
        }

        private async Task DestroyLastScene()
        {
            lastScene?.Destroy();
            lastScene = null;

            await ToSignal(GetTree(), "process_frame");
        }
    }
}
