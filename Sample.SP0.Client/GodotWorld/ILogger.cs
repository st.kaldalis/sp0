namespace Sample.SP0.Client.GodotWorld
{
    public interface ILogger
    {
        void Fatal(string message);
    }
}
