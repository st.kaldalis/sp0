﻿using Sample.SP0.Client.Core;
using Godot;
using System.Collections.Generic;

namespace Sample.SP0.Client.GodotWorld
{
    public partial class Loop : Node, ILoop
    {
        private readonly LinkedList<IUpdatable> updatables = new LinkedList<IUpdatable>();

        public void Add(IUpdatable updatable)
        {
            updatables.AddLast(updatable);
        }

        public void Remove(IUpdatable updatable)
        {
            updatables.Remove(updatable);
        }

        public override void _Process(double delta)
        {
            base._Process(delta);

            LinkedListNode<IUpdatable> node = updatables.First;
            while (node != null)
            {
                LinkedListNode<IUpdatable> nextNode = node.Next;
                node.Value.Update();
                node = nextNode;
            }
        }
    }
}
