using Sample.SP0.Client.Core.TitleScene;
using Sample.SP0.Client.Core.TitleScene.View;
using Godot;
using System;
using System.Collections.Generic;

namespace Sample.SP0.Client.GodotWorld.TitleScene.View
{
    public partial class SelectLocaleWindow : Control, ISelectLocaleWindow
    {
        public event EventHandler<LocaleButtonClickedEventArgs> LocaleButtonClickedEvent;
        public event EventHandler OKButtonClickedEvent;
        public event EventHandler ClosedEvent;

        private string selectedLocale;
        private Dictionary<string, Control> checkMarks;

        public override void _Ready()
        {
            base._Ready();

            checkMarks = new Dictionary<string, Control>();

            var okBtn = GetNode<Button>("OKButton");
            okBtn.Pressed += OnOKButtonClicked;

            var vBox = GetNode("ScrollContainer/MarginContainer/VBoxContainer");
            var locales = new List<string> { "en", "ko", "ak" };
            foreach (var locale in locales)
            {
                var btn = vBox.GetNode<Button>($"{locale}/Button");
                btn.Pressed += () => { OnLocaleButtonClicked($"{locale}"); };

                checkMarks[locale] = vBox.GetNode<Control>($"{locale}/CheckMark");
            }
        }

        private void OnLocaleButtonClicked(string locale)
        {
            LocaleButtonClickedEvent(this, new LocaleButtonClickedEventArgs { Locale = locale });
        }

        private void OnOKButtonClicked()
        {
            OKButtonClickedEvent(this, EventArgs.Empty);
        }

        public void Close()
        {
            Visible = false;

            LocaleButtonClickedEvent = (sender, args) => { };
            OKButtonClickedEvent = (sender, args) => { };

            ClosedEvent?.Invoke(this, EventArgs.Empty);
            ClosedEvent = (sender, args) => { };
        }

        public string GetSelectedLocale()
        {
            return selectedLocale;
        }

        public void SelectLocale(string locale)
        {
            selectedLocale = locale;

            SetVisibleCheckMark(selectedLocale);
        }

        private void SetVisibleCheckMark(string locale)
        {
            foreach (var entry in checkMarks)
                entry.Value.Visible = entry.Key == locale;
        }
    }
}
