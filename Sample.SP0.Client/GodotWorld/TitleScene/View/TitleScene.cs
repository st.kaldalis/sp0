using Sample.SP0.Client.Core.TitleScene.View;
using Sample.SP0.Client.GodotWorld.View;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.TitleScene.View
{
    public partial class TitleScene : Scene, ITitleScene
    {
        public event EventHandler StartButtonClickedEvent;
        public event EventHandler LocaleButtonClickedEvent;

        private WindowStack windowStack;
        private SelectLocaleWindow selectLocaleWindow;
        private Control enterButtonPanel;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            enterButtonPanel = GetNode<Control>("CanvasLayer/Control/EnterButtonPanel");

            var btn = enterButtonPanel.GetNode<Button>("Button");
            btn.Pressed += OnStartButtonPressed;

            var localeBtn = GetNode<TextureButton>("CanvasLayer/Control/LocaleButton");
            localeBtn.Pressed += OnLocaleButtonPressed;

            windowStack = GetNode<WindowStack>("CanvasLayer/WindowStack");
            selectLocaleWindow = windowStack.GetNode<SelectLocaleWindow>("SelectLocaleWindow");

            var versionLabel = GetNode<Label>("CanvasLayer/Control/VersionLabel");
            versionLabel.Text = GetVersion();
        }

        private string GetVersion()
        {
            if (OS.GetName() == "Android")
            {
                var configfile = new ConfigFile();
                var err = configfile.Load("res://export_presets.cfg");
                if (err == Error.Ok)
                    return (string)configfile.GetValue("preset.0.options", "version/name");
            }

            return string.Empty;
        }

        private void OnLocaleButtonPressed()
        {
            LocaleButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        public void OnStartButtonPressed()
        {
            StartButtonClickedEvent?.Invoke(this, EventArgs.Empty);
        }

        public ISelectLocaleWindow OpenSelectLocaleWindow()
        {
            selectLocaleWindow.Visible = true;
            selectLocaleWindow.ClosedEvent += (sender, args) => { windowStack.OnWindowClosed(selectLocaleWindow); };

            windowStack.OnWindowOpened(selectLocaleWindow);

            return selectLocaleWindow;
        }

        public void ShowStartButton()
        {
            enterButtonPanel.Visible = true;
        }
    }
}
