﻿using Sample.SP0.Client.Core.TitleScene;
using Godot;

namespace Sample.SP0.Client.GodotWorld.TitleScene
{
    public class TextSet : ITextSet
    {
        public void SetLocale(string locale)
        {
            TranslationServer.SetLocale(locale);
        }
    }
}
