﻿using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.View
{
    public partial class WindowStack : Control
    {
        private Control windowBackground;

        public override void _Ready()
        {
            base._Ready();

            windowBackground = GetNode<Control>("WindowBackground");
        }

        public void OnWindowOpened(Control window)
        {
            window.MoveToFront();

            UpdateWindowBackground();
        }

        public void OnWindowClosed(Control window)
        {
            MoveChild(window, 0);

            UpdateWindowBackground();
        }

        private void UpdateWindowBackground()
        {
            var children = GetChildren();
            for (var index = children.Count - 1; index >= 0; index--)
            {
                var child = children[index] as Control;
                if (child == null) continue;

                var isVisibleWindow = windowBackground.GetInstanceId() != child.GetInstanceId() && child.Visible;
                if (isVisibleWindow)
                {
                    windowBackground.Visible = true;
                    MoveChild(windowBackground, index - 1);
                    return;
                }
            }
            windowBackground.Visible = false;
        }
    }
}
