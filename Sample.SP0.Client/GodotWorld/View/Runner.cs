using Sample.SP0.Client.Core.RaceScene.View;
using Godot;
using System;

namespace Sample.SP0.Client.GodotWorld.View
{
	public partial class Runner : Node2D, IRunner
	{
		public event EventHandler DestroyedEvent;

		public string RunnerName;

		private float speed;

		private AnimationPlayer animationPlayer;

		public override void _Ready()
		{
			base._Ready();

			animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
		}

		public float GetSpeed()
		{
			return speed;
		}

		public void Run(float speed)
		{
			this.speed = speed;

			animationPlayer.CurrentAnimation = "Run";
		}

		public void Wait()
		{
			speed = 0.0f;

			animationPlayer.CurrentAnimation = "Wait";
		}

		public override void _Process(double delta)
		{
			base._Process(delta);

			Position = new Vector2(Position.X + speed * (float)delta, Position.Y);
		}

		public override void _ExitTree()
		{
			base._ExitTree();

			DestroyedEvent?.Invoke(this, EventArgs.Empty);
			DestroyedEvent = (sender, args) => { };
		}
	}
}
