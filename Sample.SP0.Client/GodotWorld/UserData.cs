﻿using System.Threading.Tasks;

namespace Sample.SP0.Client.GodotWorld
{
    public class UserData : Core.UserData
    {
        private readonly IServerApi serverApi;
        private readonly ILogger logger;

        public UserData(IServerApi serverApi, ILogger logger)
        {
            this.serverApi = serverApi;
            this.logger = logger;
        }

        public override async Task Load()
        {
            var getWalletResult = await serverApi.GetWallet();
            if (!getWalletResult.IsSuccess)
            {
                logger.Fatal("Failed to get wallet result.");
                return;
            }

            Wallet.Coin = getWalletResult.Coin;
        }

        public override async Task Reset()
        {
            await serverApi.RemoveUser();

            Wallet.Coin = 0;

            await Load();
        }
    }
}
